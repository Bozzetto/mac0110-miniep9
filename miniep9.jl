function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)

    if p == 0
        return 1
    elseif p == 1
        return M
    elseif p >= 2
        aux = multiplica(M,M)
        for i in 3:p
            aux = multiplica(aux,M)
        end
        return aux
    end
end

function matrix_pot_by_squaring(M,p)

    if p == 1
        return M
    elseif p >= 2
        if p%2 == 0
            return matrix_pot_by_squaring(multiplica(M,M),p/2)
        else
            return multiplica(M,matrix_pot_by_squaring(multiplica(M,M),(p-1)/2))
        end
    end
end

function compareTimes()

    M = [5 1 2 ; 8 9 2 ; 3 2 3]

    @time matrix_pot(M,200)
    @time matrix_pot_by_squaring(M,200)

end
